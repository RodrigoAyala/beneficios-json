
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/beneficio.json', function(req,res) {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('./beneficios.db');
  var items = Array();

  db.serialize(function(){
    db.all("SELECT * FROM beneficio", function(err,row){
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify(row));

      });
  });
  db.close();
});

app.get('/categoria.json', function(req,res) {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('./beneficios.db');
  var items = Array();

  db.serialize(function(){
    db.all("SELECT * FROM categoria", function(err,row){
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify(row));

      });
  });
  db.close();
});

app.get('/subcategoria.json', function(req,res) {
  var sqlite3 = require('sqlite3').verbose();
  var db = new sqlite3.Database('./beneficios.db');
  var items = Array();

  db.serialize(function(){
    db.all("SELECT * FROM subcategoria", function(err,row){
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify(row));

      });
  });
  db.close();
});

app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
